# Planning Poker by Berra

## Dev and build requirements

Known issues when installing and running node.js < 4.6. Tested and working on 6.2.2 and 6.7.0.

* node.js >= 6.2.2

## Installation

    $ npm install

## Dev server

This starts the webpack-dev-server at port 3000. Open http://localhost:3000

    $ npm run dev

## Create build

This creates a complete build to ./build folder.

    $ npm run build

## Run a simple http server

From the build folder, with python, at port 8000. Open http://localhost:8000

    $ cd ./build
    $ python -m SimpleHTTPServer 8000