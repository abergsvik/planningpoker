/*
 * --------------------------------------------------------
 * Oh, hi! Welcome!
 *
 * This our main app container
 *
 * What <App> does is load some dump old component
 * containing our UI.
 * It also has our app "state" passed in as props from
 * main.js
 * Then the props are passed down to whatever
 * the components need.
 *
 * Could of course been handled by a cool state manager
 * like redux. But a little overkill for this application.
 * It will be a TODO when evolving to a "real" app
 * with proper login and multiple users doing actions in
 * real time.
 *
 * --------------------------------------------------------
*/

/*
 * Import from node_modules
 */
import React, { Component } from 'react'
import update from 'react-addons-update'


/*
 * Import dumb components
 */
import Card from './Components/Card'
import Playboard from './Components/Playboard'
import Button from './Components/Button'
import Sidebar from './Components/Sidebar'
import Icon from './Components/Icon'

/*
 * Import lib functions
 */
import { random } from './lib/random'

/*
 *	We import the master scss that holds our import file - variables if we had any.
 */
require('./style/master.scss')


class App extends Component {

	constructor(props) {
    super(props)
    // Bind functions
    this.cardClick = this.cardClick.bind(this)
    this.reveal = this.reveal.bind(this)
    this.fakeUserWait = this.fakeUserWait.bind(this)
    this.selectCard = this.selectCard.bind(this)
    this.userSelection = this.userSelection.bind(this)
    this.newSession = this.newSession.bind(this)
    // Initial application state
    this.state = props.initialState
  }

  componentDidMount() {
  	// the other users start to "pick"
  	// their cards when the application mounts.
  	this.fakeUserWait()
  }

  userSelection(index) {
  	// get me a random number between 1 and the amounts of cards...
  	let randomIndex = random(1, this.props.conf.cards.length-1)
  	// this will be the card that the user "chooses"
  	let value = this.props.conf.cards[randomIndex]
  	// and we update the user state object
  	let users = update(this.state.users, {
  		[index]: {
  			selection: {$set: value}
  		}
  	})
  	this.setState({
  		users
  	})
  }

  fakeUserWait() {
  	// iterate the users
  	for(let index in this.state.users) {
  		// and when the time is right, they select a card
  		setTimeout(this.userSelection, index*1000, index)
  	}
	}


	cardClick(card) {
		// this is what happens when we click a card
		// if we're in the SELECT phase - select the card!
		if(this.state.phase === this.state.phases.SELECT) {
			this.selectCard(card)
		} else if(this.state.phase === this.state.phases.WAITING){
		// and if we're in the WAITING phase - the it is time to go to the
		// REVEAL phase.
			this.reveal()
		} else if (this.state.phase === this.state.phases.REVEAL){
		// of course, if we're in the REVEAL phase - we're done!
		// start a new session...
			this.newSession()
		}

	}

	selectCard(card) {
		// Pick a card, just any card! Ah, the `card` you clicked! Good choice!
		this.setState({
			selectedCard: card,
			phase: this.state.phases.WAITING
		})
	}

	reveal() {
		/*
			The moment of truth!
			This is where we reveal our cards.
			And yes. We could wait for everyone to finnish.
			But usually HarryB is getting a coffe.
			So the other players can put in cards even after the reveal phase is active.
		*/
		this.setState({
			phase: this.state.phases.REVEAL
		})
	}

	newSession() {
		// First we set the users to initial state (no cards selected)
		// no need to reset to complete initial stage at this point
		// What if we're in the future would like to save other things in state
		// like the stories we already played and things like that.
		let state = update(this.state, {
			users: {$set: this.props.initialState.users}
		})
		// then we update the state
		this.setState({
			phase: this.state.phases.SELECT,
			users: state.users,
			selectedCard: null
		})
		// let the others start picking their cards
		this.fakeUserWait()
	}

	render() {
		const _this = this // scope fix
		return <div className='wrapper'>
				<Playboard>
					{this.props.conf.cards.map(function (i, index) {
						return <Card style={{animationDuration: index/8 + 0.5 + 's'}} phase={_this.state.phase} selected={_this.state.selectedCard === i} onClick={_this.cardClick.bind(false, i)} key={i} value={i} />
					})}
				</Playboard>
				<Sidebar selectedCard={this.state.selectedCard} phase={this.state.phase} user={this.state.user} users={this.state.users}>
					<h1 className='app-title'>Planning Poker by Berra</h1>
					<Button disabled={this.state.phase !== 'WAITING'} onClick={this.reveal}>
						<Icon name='eye' />
						REVEAL
					</Button>
					<Button disabled={this.state.phase === 'SELECT'} onClick={this.newSession}>
						<Icon name='refresh' />
						NEW
					</Button>

				</Sidebar>
			</div>
	}

}

export default App