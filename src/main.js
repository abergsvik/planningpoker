import 'babel-polyfill'

import React from 'react'
import ReactDOM from 'react-dom'

import App from './App'

/*
	Our applications state sent via props to application
*/
const initialState = {
	phase: 'SELECT',
	// Define phases for easier debugging and prevent type errors.
	phases: {
		SELECT: 'SELECT',
		WAITING: 'WAITING',
		REVEAL: 'REVEAL'
	},
	user: "Guest",
	selectedCard: null,
	// Some other fake users
	users: [
		{id: 1, name: "Mimi", selection: null},
		{id: 2, name: "MickeJ", selection: null},
		{id: 3, name: "MsM.", selection: null},
		{id: 4, name: "HarryB.", selection: null}
	]
}

/*
	Configuration variables, send to application via props.
*/
const configuration = {
	cards: [0,'½',1,2,3,5,8,13,20,40,100,'?']
}

ReactDOM.render(
  <App initialState={initialState} conf={configuration} />,
  document.getElementById('app')
);
