/*

	Sidebar!
	Yeah - it is on the edge of being to big for a function component.
	But the logic is quite straight forward and all the props
	coming from our main App.js


	Properties:
	phase: <string> (required) // the current phase of the game
	users: <array> // the other users in the game
	user: <string> (required) // current user (also known as guest until we add a login)

	Style: sidebar.scss

*/

import React, { PropTypes } from 'react'

import Icon from './Icon'

const Sidebar = (props) => {
	let userStatus = props.phase === 'SELECT' ? '' : props.phase === 'REVEAL' ? props.selectedCard : <Icon name='check' />
	return (<div className='sidebar'>
						{props.children}
						<div className='users'>
							<div className='user me'>
								<Icon style={{marginRight: '.5rem'}} name='heart' />
								{props.user} {userStatus}
							</div>
							{props.users.map(function (user, arrayIndex) { // Loop users and return name and status
								let status = null
								// if reveal phase, show number
								if(props.phase === 'REVEAL'){
									status = user.selection
								}	else {
									status = !user.selection ? <Icon name='clock-o' /> : <Icon name='check' />
								}
								return <div className='user' key={arrayIndex}>
												<Icon style={{marginRight: '.5rem'}} name='user' />
												{user.name} {status}
											 </div>
							})}
						</div>
					</div>)
}

// Make sure we don't map an undefined users array
// I also add the "guest" user as a default for the current user. It is required you know!
// But - it is hard coded in main.js so I don't worry much.
Sidebar.defaultProps = {
	users: [],
	user: 'Guest'
}

// Define our props and types
Sidebar.propTypes = {
	phase: PropTypes.string.isRequired,
	users: PropTypes.array,
	user: PropTypes.string.isRequired
}

export default Sidebar