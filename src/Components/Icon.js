/*

	A reusable icon component utilizing the Font Awesome icon font.

	Properties:
	name: <string> (required) // yes, we do need an icon name to show an icon
	style: <object> (optional) // optional styling added here
	spin: <bool> (optional) // should the icon spin?

	Style: icon.scss

*/

// We import React to be able to create our dumb little component
// We also need the defined proptypes from the react lib.
import React, { PropTypes } from 'react'

// A pretty little declaration of our component function
const Icon = (props) => {
	// We could use a Icon.defaultProps = {} as well.
	let style = props.style || {}
	// Should it spin?
	let spinningClass = props.spin ? 'fa-spin' : '' // uh, exhausting! ;)
	return <i style={style} className={`fa ${spinningClass} fa-${props.name}`}></i>
}

Icon.propTypes = {
	name: PropTypes.string.isRequired,
	style: PropTypes.object,
	spin: PropTypes.bool
}

export default Icon