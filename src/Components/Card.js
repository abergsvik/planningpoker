/*

	The card element.
	Position and layout changes with css classes
	based on phase and selection sent via props.

	Properties:
	selected: <bool> // TRUE if THIS is the selected card
	phase: <string> // The phase we're currently on
	value: <string>,<number> // The card's value
	onClick: <function> // clickhandler


	Style: card.scss

*/

import React, { PropTypes } from 'react'

const Card = (props) => {
	let cardClass = ' card ' // always use default class
	if(props.selected) {
		cardClass += ' selected-card '
		if(props.phase === 'WAITING') {
			cardClass += ' back '
		}
	}
	else if(props.phase === 'WAITING' || props.phase === 'REVEAL') {
		cardClass += ' hidden back '
	}
	cardClass += ' ' + props.phase + ' ' // add the phase for animation purposes

	// let phaseClass = props.phase === 'WAITING' && !props.selected ? ' back hidden ' : ''
	return (<div style={props.style} onClick={props.onClick} className={cardClass}>
						<span>{props.value}</span>
						<div onClick={props.onClick} className='backside'></div>
					</div>)
}

Card.defaultProps = {
	style: {}
}

Card.propTypes = {
	selected: PropTypes.bool,
	phase: PropTypes.string.isRequired,
	value: PropTypes.oneOfType([
		PropTypes.string,
		PropTypes.number
	]),
	onClick: PropTypes.func
}

export default Card