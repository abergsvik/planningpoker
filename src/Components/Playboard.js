/*

	The playboard. This container with flex layout has our cards (props.children)

	Style: playboard.scss

*/

import React from 'react'

const Playboard = (props) => {
	return (<div className='playboard'>{props.children}</div>)
}

export default Playboard