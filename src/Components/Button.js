/*

	A generic button element.

	Properties:
	disabled: <bool> (optional) // If the button is disabled, style it different and prevent click
	onClick: <function> (required) // Pass the clickhandler to this property

	Style: button.scss

*/

import React, { PropTypes } from 'react'

const Button = (props) => {
	return (<button disabled={props.disabled} onClick={!props.disabled ? props.onClick : null} className='button'>{props.children}</button>)
}

Button.propTypes = {
	disabled: PropTypes.bool,
	onClick: PropTypes.func.isRequired
}

export default Button