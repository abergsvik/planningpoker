
const path = require('path')
const webpack = require('webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const HtmlWebpackPlugin = require('html-webpack-plugin')

// post css "plugins"
const autoprefixer = require('autoprefixer')

module.exports = {
  entry: {
    app: ['./src/main.js']
  },
  output: {
    path: './build',
    filename: '[name].js'
  },
  module: {
    loaders: [
      {
        test: /\.(css|scss)$/,
        loader: 'style!' + 'css?sourceMap' + '!postcss-loader' + '!sass?sourceMap'
      },
      {
        loader: 'babel-loader',
        test: /\.(js|jsx)$/,
        exclude: [
          path.resolve(__dirname, 'node_modules')
        ],
        include: [
          path.resolve(__dirname, 'src')
        ],
        query: {
          plugins: ['transform-runtime'], // 'lodash', 'system-import-transformer',
          presets: ['es2015', 'react']
        }
      },
      {
        test: /\.(json)$/,
        exclude: [/node_modules/],
        loader: 'json-loader'
      },
      {
        test: /\.png$/, loader: "url-loader"
      },
      {
        test: /\.(svg|ttf|woff|woff2|eot)(\?v=\d+\.\d+\.\d+)?$/,
        loader: 'url-loader'
      }
    ]
  },
  postcss: function () {
    return [autoprefixer]
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: 'index.html'
    }),
    new CopyWebpackPlugin([
      {
        from: './static/',
        to: './static/'
      }
    ]),
    new webpack.DefinePlugin({
      'process.env': {
        'NODE_ENV': JSON.stringify('development'),
        'npm_package_version': JSON.stringify(process.env.npm_package_version),
        'npm_package_name': JSON.stringify(process.env.npm_package_name),
        'npm_package_description': JSON.stringify(process.env.npm_package_description)
      }
    })
  ],

  devtool: 'source-map'

}
